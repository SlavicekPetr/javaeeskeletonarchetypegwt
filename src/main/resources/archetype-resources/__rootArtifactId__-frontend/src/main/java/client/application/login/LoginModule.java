#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.application.login;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import ${package}.client.application.security.CurrentSession;

/**
 *
 * @author slavicekp
 */
public class LoginModule extends AbstractPresenterModule {
    @Override
    protected void configure() {		
        bindPresenter(LoginPresenter.class, LoginPresenter.MyView.class, LoginView.class,
                LoginPresenter.MyProxy.class);
    }
}
