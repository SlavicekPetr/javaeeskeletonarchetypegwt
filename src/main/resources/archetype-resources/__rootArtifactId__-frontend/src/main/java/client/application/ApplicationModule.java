#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.application;

import ${package}.client.application.home.HomeModule;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import ${package}.client.application.login.LoginModule;
import ${package}.client.application.security.CurrentSession;
import ${package}.client.application.security.SecurityModule;

public class ApplicationModule extends AbstractPresenterModule {

	@Override
	protected void configure() {
		install(new LoginModule());
		install(new HomeModule());
		
		bind(CurrentSession.class).asEagerSingleton();
		
		bindPresenter(ApplicationPresenter.class, ApplicationPresenter.MyView.class, ApplicationView.class,
				ApplicationPresenter.MyProxy.class);
	}
}
