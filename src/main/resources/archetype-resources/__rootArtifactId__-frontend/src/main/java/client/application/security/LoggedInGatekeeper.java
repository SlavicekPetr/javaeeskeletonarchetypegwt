#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.application.security;

import com.google.inject.Inject;
import com.gwtplatform.mvp.client.annotations.DefaultGatekeeper;
import com.gwtplatform.mvp.client.proxy.Gatekeeper;

/**
 *
 * @author slavicekp
 */
@DefaultGatekeeper
public class LoggedInGatekeeper implements Gatekeeper {

	private final CurrentSession currentSession;
    
	@Inject
    public LoggedInGatekeeper(CurrentSession currentSession) {
        this.currentSession = currentSession;
    }

    @Override
    public boolean canReveal() {
        return currentSession.isLoggedIn();
    }
}
