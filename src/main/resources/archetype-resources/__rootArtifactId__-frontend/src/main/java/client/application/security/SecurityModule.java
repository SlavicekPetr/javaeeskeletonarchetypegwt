#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.application.security;

import com.google.gwt.inject.client.AbstractGinModule;
import javax.inject.Singleton;

/**
 *
 * @author slavicekp
 */
public class SecurityModule extends AbstractGinModule {
    @Override
    protected void configure() {
        bind(LoggedInGatekeeper.class).in(Singleton.class);
		bind(CurrentSession.class).asEagerSingleton();
    }
}
