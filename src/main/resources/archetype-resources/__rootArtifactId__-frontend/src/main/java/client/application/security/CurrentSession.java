#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.application.security;

/**
 *
 * @author slavicekp
 */
public class CurrentSession {

	private Boolean loggedIn;

	public CurrentSession() {
		loggedIn = false;
	}

	public void reset() {
		setLoggedIn(false);
	}

	public Boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

}
