#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.application.login;

import com.gwtplatform.mvp.client.UiHandlers;

/**
 *
 * @author slavicekp
 */
public interface LoginUiHandlers extends UiHandlers {
	
	 void onLogin(String username, String password);

}
