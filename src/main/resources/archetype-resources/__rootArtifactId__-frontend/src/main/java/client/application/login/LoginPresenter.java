#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.application.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dev.util.HttpHeaders;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.common.net.MediaType;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;
import ${package}.client.application.ApplicationPresenter;
import ${package}.client.place.NameTokens;
import ${package}.client.application.security.CurrentSession;

/**
 *
 * @author slavicekp
 */
public class LoginPresenter extends Presenter<LoginPresenter.MyView, LoginPresenter.MyProxy>
		implements LoginUiHandlers {

	private final PlaceManager placeManager;

	@ProxyStandard
	@NameToken(NameTokens.LOGIN)
	@NoGatekeeper
	interface MyProxy extends ProxyPlace<LoginPresenter> {
	}

	public interface MyView extends View, HasUiHandlers<LoginUiHandlers> {
	}

	private final CurrentSession currentSession;

	@Inject
	LoginPresenter(
			EventBus eventBus,
			MyView view,
			MyProxy proxy,
			PlaceManager placeManager,
			CurrentSession currentSession) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);

		this.placeManager = placeManager;
		getView().setUiHandlers(this);
		this.currentSession = currentSession;
	}

	@Override
	public void onLogin(final String username, final String password) {

		//call protected place first before login form is sent
		String url = GWT.getHostPageBaseURL() + "protected/protectedText.txt";
		RequestBuilder rb = new RequestBuilder(RequestBuilder.GET, url);

		rb.setCallback(new RequestCallback() {
			@Override
			public void onResponseReceived(Request request, Response response) {
				RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, URL.encode(GWT.getHostPageBaseURL() + "j_security_check"));
				builder.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.FORM_DATA.toString());
				StringBuilder sb = new StringBuilder();
				sb.append("j_username=");
				sb.append(URL.encodeQueryString(username));
				sb.append("&j_password=");
				sb.append(URL.encodeQueryString(password));
				try {
					builder.sendRequest(sb.toString(), new RequestCallback() {
						@Override
						public void onResponseReceived(Request request, Response response) {
							currentSession.setLoggedIn(Boolean.TRUE);
							redirectAfterLogin();
						}

						@Override
						public void onError(Request request, Throwable exception) {
							Window.alert("Login failed");
						}

					});
				} catch (RequestException e) {

					//display error
				}
			}

			@Override
			public void onError(Request request, Throwable caught) {
				throw new UnsupportedOperationException("Not supported yet.");
			}
		});

		try {
			rb.send();
		} catch (RequestException ex) {
			// TODO present the error in a text area or something 
		}

	}

	private void redirectAfterLogin() {
		if (History.getToken().isEmpty() || History.getToken().equals(NameTokens.LOGIN)) {
			// redirect to home
			PlaceRequest homePlaceRequest = new PlaceRequest.Builder().nameToken(NameTokens.HOME).build();
			placeManager.revealPlace(homePlaceRequest);
		} else {
			// redirect "back (i.e. original page)
			placeManager.revealCurrentPlace();
		}
	}
}
