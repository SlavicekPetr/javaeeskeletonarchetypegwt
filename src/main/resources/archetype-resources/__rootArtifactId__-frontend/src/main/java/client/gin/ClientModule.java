#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.client.gin;

import ${package}.client.application.ApplicationModule;
import ${package}.client.place.NameTokens;
import ${package}.client.resources.ResourceLoader;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.gin.DefaultModule;
import ${package}.client.application.login.LoginModule;
import ${package}.client.application.security.SecurityModule;

public class ClientModule extends AbstractPresenterModule {

	@Override
	protected void configure() {
		install(new DefaultModule.Builder()
				.defaultPlace(NameTokens.LOGIN)
				.errorPlace(NameTokens.LOGIN)
				.unauthorizedPlace(NameTokens.LOGIN)
				.build());

		install(new SecurityModule());		
		install(new ApplicationModule());

		bind(ResourceLoader.class).asEagerSingleton();
		
		
	}
}
