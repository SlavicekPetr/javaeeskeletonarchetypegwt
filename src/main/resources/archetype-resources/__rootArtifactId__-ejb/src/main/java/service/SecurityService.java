#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service;

import ${package}.interfaces.ISecurityService;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author slavicekp
 */
@Singleton
@Startup
public class SecurityService implements ISecurityService {

	@PersistenceContext
	EntityManager em;
	
		
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityService.class);
	

	
}
